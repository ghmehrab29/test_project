import 'dart:async';
import 'package:get/get.dart';
import 'package:speech_to_text/speech_recognition_result.dart';
import 'package:speech_to_text/speech_to_text.dart';
import 'package:translator/translator.dart';

class VoiceAssistantController extends GetxController {
  final SpeechToText _speechToText = SpeechToText();
  StreamController<String> command = StreamController();
  RxBool isWave = RxBool(false);
  RxString speechText = RxString('هنوز صحبت نکردید');
  RxBool isTranslate = RxBool(false);
  RxString simpleText = RxString('');
  RxString translatedText = RxString('');
  RxString currentLocale = RxString('en_US');
  var val = 0.0.obs;
  Timer? timer;
  RxList<Map<String, String>> locales = RxList([]);
  RxBool isTranlating = RxBool(false);

  @override
  void onInit() async {
    super.onInit();
    await _initSpeech();
    await createLocales();
  }

  Future<void> createLocales() async {
    List<LocaleName> allLocales = await _speechToText.locales();
    locales.value = allLocales
        .map(
          (e) => {
            'name': e.name,
            'value': e.localeId,
          },
        )
        .toList();
    locales.refresh();
  }

  Future<void> _initSpeech() async {
    await _speechToText.initialize(
      onError: (errorNotification) {
        if (errorNotification.errorMsg == 'error_speech_timeout' ||
            errorNotification.errorMsg == 'error_no_match') {
          if (!_speechToText.isListening) {
            stopListening();
            startListening();
          }
        }
      },
      onStatus: (status) {},
    );
  }

  void setCommandController(StreamController<String> controller) {
    command = controller;
  }

  void startListening() async {
    await _speechToText.listen(
      localeId: 'fa_IR',
      onResult: _onSpeechResult,
    );
    timer ??= Timer.periodic(
        const Duration(milliseconds: 50), (timer) => _refreshValue());
  }

  void stopListening() async {
    await _speechToText.stop();
    if (timer != null) {
      timer!.cancel();
      timer = null;
    }
  }

  void tranlateLocaleChanged() {
    isTranlating.value = true;
    isTranlating.refresh();
    GoogleTranslator()
        .translate(
      simpleText.value,
      from: 'fa',
      to: currentLocale.value.split('_')[0],
    )
        .then((s) {
      translatedText.value = s.text;
      translatedText.refresh();
      isTranlating.value = false;
      isTranlating.refresh();
    });
  }

  void _onSpeechResult(SpeechRecognitionResult result) {
    command.add(result.recognizedWords);
    speechText.value = result.recognizedWords;
    speechText.refresh();
  }

  void _refreshValue() {
    val.value = _speechToText.lastSoundLevel.abs() * 5.5 + 50;
    val.refresh();
  }
}
