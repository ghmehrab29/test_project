import 'package:get/get.dart';
import 'package:test_project/model/location_model.dart';
import 'package:test_project/model/simple_weather.dart';
import 'package:test_project/model/weather.dart';
import 'package:test_project/service/remote_service.dart';

class WeatherController extends GetxController {
  Rx<Weather> currentWeather = Rx(Weather(
    simpleWeather: const SimpleWeather(
      temperature: 0,
      humidity: 0,
      windSpeed: 0,
      isDay: true,
    ),
    location: const LocationModel(
      country: '',
      city: '',
      localtimeEpoch: 0,
      lat: 0.0,
      lon: 0.0,
    ),
  ));

  @override
  void onInit() {
    updateWeather('Tehran');
    super.onInit();
  }

  void updateWeather(String city) async {
    var temp = await RemoteService.getCityWeather(city);
    temp.fold((l) {
      currentWeather.value = l;
      currentWeather.refresh();
      update();
    }, (r) {});
  }
}
