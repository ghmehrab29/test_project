import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:dartz/dartz.dart';
import 'package:test_project/model/weather.dart';

class RemoteService {
  static const String TOKEN = '141d81575d92e713adef350c2d5dccd9';
  static const String BASE_URL = 'http://api.weatherstack.com';

  static Future<Either<Weather, String>> getCityWeather(String city) async {
    try {
      Uri uri =
          Uri.parse('$BASE_URL/current?access_key=$TOKEN&query=$city&units=m');
      var response = await http.get(
        uri,
      );
      if (response.statusCode == 200) {
        var data = jsonDecode(response.body);
        return Left(Weather.fromJson(data));
      } else {
        return Right(response.statusCode.toString());
      }
    } catch (ex) {
      return const Right('Something went wrong');
    }
  }
}
