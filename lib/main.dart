import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_project/controller/voice_assistant_controller.dart';
import 'package:test_project/controller/weather_controller.dart';
import 'package:test_project/screen/home_page.dart';

void main() async {
  Get.put(VoiceAssistantController());
  Get.put(WeatherController());
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final VoiceAssistantController voiceAssistantController =
        Get.find<VoiceAssistantController>();
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: Obx(
        () => Scaffold(
          appBar: AppBar(
            actions: [
              voiceAssistantController.isTranslate.value
                  ? IconButton(
                      onPressed: () {
                        voiceAssistantController.isTranslate.value = false;
                        voiceAssistantController.isTranslate.refresh();
                      },
                      icon: const Icon(Icons.map),
                    )
                  : IconButton(
                      onPressed: () {
                        voiceAssistantController.isTranslate.value = true;
                        voiceAssistantController.isTranslate.refresh();
                      },
                      icon: const Icon(Icons.translate),
                    ),
            ],
          ),
          body: const SafeArea(
            child: HomePage(),
          ),
        ),
      ),
    );
  }
}
