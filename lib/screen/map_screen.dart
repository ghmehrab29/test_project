import 'package:flutter/material.dart';
import 'package:flutter_osm_plugin/flutter_osm_plugin.dart';
import 'package:get/get.dart';
import 'package:test_project/controller/weather_controller.dart';
import 'package:test_project/widget/weather_card.dart';

class MapScreen extends StatelessWidget {
  const MapScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final MapController mapController = MapController();
    GeoPoint prevPoint = GeoPoint(latitude: 35, longitude: 51);
    bool firstTime = true;
    final WeatherController weatherController = Get.find<WeatherController>();
    return Obx(
      () {
        weatherController.currentWeather.value;
        if (!firstTime) {
          GeoPoint newPoint = GeoPoint(
              latitude: weatherController.currentWeather.value.location.lat,
              longitude: weatherController.currentWeather.value.location.lon);
          try {
            mapController.removeMarker(prevPoint);
            mapController.addMarker(newPoint,
                markerIcon: const MarkerIcon(
                  icon: Icon(
                    Icons.location_on_rounded,
                    color: Colors.red,
                    size: 80,
                  ),
                ));
            mapController.goToLocation(newPoint);
            mapController.setZoom(zoomLevel: 9, stepZoom: 4);
            prevPoint = newPoint;
          } catch (ex) {}
        }
        firstTime = false;
        return Column(
          children: [
            SizedBox(
              height: size.height * .025,
            ),
            WeatherCard(
              weather: weatherController.currentWeather.value,
            ),
            SizedBox(
              height: size.height * .035,
            ),
            Expanded(
              child: Container(
                width: size.width * .85,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: OSMFlutter(
                    initZoom: 9,
                    controller: mapController,
                    roadConfiguration: RoadConfiguration(
                      roadColor: Colors.yellowAccent,
                    ),
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
