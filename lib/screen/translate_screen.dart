import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_project/controller/voice_assistant_controller.dart';
import 'package:test_project/widget/locale_drop_down.dart';

class TranslateScreen extends StatelessWidget {
  const TranslateScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final VoiceAssistantController voiceAssistantController =
        Get.find<VoiceAssistantController>();
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Obx(
        () => Column(
          children: [
            Expanded(
              child: Container(
                padding: const EdgeInsets.all(10),
                width: size.width,
                decoration: BoxDecoration(
                  color: Color(0xFFEF4444).withOpacity(.5),
                  border: Border.all(
                    width: 1.5,
                    color: Color(0xFFEF4444),
                  ),
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Text(
                  voiceAssistantController.simpleText.value,
                  textAlign: TextAlign.right,
                  style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 14,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: size.height * .035,
            ),
            Expanded(
              child: Container(
                padding: const EdgeInsets.all(10),
                width: size.width,
                decoration: BoxDecoration(
                  color: Color(0xFFEF4444).withOpacity(.5),
                  border: Border.all(
                    width: 1.5,
                    color: Color(0xFFEF4444),
                  ),
                  borderRadius: BorderRadius.circular(20),
                ),
                child: voiceAssistantController.isTranlating.value
                    ? const Center(
                        child: CircularProgressIndicator(
                          color: Color(0xFFEF4444),
                        ),
                      )
                    : Text(
                        voiceAssistantController.translatedText.value,
                        textAlign: TextAlign.left,
                        style: const TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                        ),
                      ),
              ),
            ),
            SizedBox(
              height: size.height * .035,
            ),
            LocaleDropDown(
              currentValue: voiceAssistantController.currentLocale.value,
              values: voiceAssistantController.locales,
              onSelected: (val) {
                voiceAssistantController.currentLocale.value = val;
                voiceAssistantController.tranlateLocaleChanged();
              },
            ),
          ],
        ),
      ),
    );
  }
}
