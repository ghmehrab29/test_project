import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_project/screen/map_screen.dart';
import 'package:test_project/screen/translate_screen.dart';
import 'package:test_project/widget/voice_assistant_button.dart';
import 'package:test_project/controller/voice_assistant_controller.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final VoiceAssistantController voiceAssistantController =
        Get.find<VoiceAssistantController>();
    return Container(
      width: size.width,
      height: size.height,
      color: Colors.white,
      child: Center(
        child: Column(
          children: [
            Obx(
              () => Expanded(
                child: voiceAssistantController.isTranslate.value
                    ? const TranslateScreen()
                    : const MapScreen(),
              ),
            ),
            VoiceAssistantButton(
              initRadius: 200,
              color: Colors.red,
              voiceAssistantController: voiceAssistantController,
            ),
          ],
        ),
      ),
    );
  }
}
