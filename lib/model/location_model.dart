class LocationModel {
  final String country;
  final String city;
  final num localtimeEpoch;
  final double lat;
  final double lon;
  const LocationModel({
    required this.country,
    required this.city,
    required this.localtimeEpoch,
    required this.lat,
    required this.lon,
  });
  factory LocationModel.fromJson(Map<String, dynamic> json) {
    final country = json['country'] ?? '';
    final city = json['name'] ?? '';
    final localtimeEpoch = json['localtime_epoch'] ?? 0;
    final lat = double.parse(json['lat'] ?? '0.0');
    final lon = double.parse(json['lon'] ?? '0.0');
    return LocationModel(
      country: country,
      city: city,
      localtimeEpoch: localtimeEpoch,
      lat: lat,
      lon: lon,
    );
  }
}
