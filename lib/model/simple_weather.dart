class SimpleWeather {
  final num temperature;
  final num humidity;
  final num windSpeed;
  final bool isDay;

  const SimpleWeather({
    required this.temperature,
    required this.humidity,
    required this.windSpeed,
    required this.isDay,
  });

  factory SimpleWeather.fromJson(Map<String, dynamic> json) {
    final temperature = json['temperature'] ?? 0;
    final humidity = json['humidity'] ?? '';
    final windSpeed = json['wind_speed'] ?? '';
    final isDay = (json['is_day'] ?? '') == 'yes';
    return SimpleWeather(
        temperature: temperature,
        humidity: humidity,
        windSpeed: windSpeed,
        isDay: isDay);
  }
}
