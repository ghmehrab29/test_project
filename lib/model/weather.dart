import 'package:test_project/model/location_model.dart';
import 'package:test_project/model/simple_weather.dart';

class Weather {
  final SimpleWeather simpleWeather;
  final LocationModel location;
  Weather({required this.simpleWeather, required this.location});
  factory Weather.fromJson(Map<String, dynamic> json) {
    final simpleWeather = SimpleWeather.fromJson(json['current'] ?? '');
    final location = LocationModel.fromJson(json['location'] ?? '');
    return Weather(simpleWeather: simpleWeather, location: location);
  }
}
