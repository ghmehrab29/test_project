import 'package:flutter/material.dart';
import 'package:test_project/model/weather.dart';

class WeatherCard extends StatelessWidget {
  const WeatherCard({
    required this.weather,
    Key? key,
  }) : super(key: key);
  final Weather weather;
  @override
  Widget build(BuildContext context) {
    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(
        weather.location.localtimeEpoch.toInt() * 1000);
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width * .85,
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: const Color(0xFFEF4444),
        borderRadius: BorderRadius.circular(20),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Icon(
                Icons.location_on_rounded,
                color: Colors.white,
              ),
              const SizedBox(
                width: 5,
              ),
              Text(
                '${weather.location.country} ${weather.location.city}',
                style: getStyle(),
              ),
            ],
          ),
          const SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Icon(
                    Icons.thermostat,
                    color: Colors.white,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Text(
                    'Temp : ${weather.simpleWeather.temperature}',
                    style: getStyle(),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Icon(
                    Icons.water_drop_rounded,
                    color: Colors.white,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Text(
                    'Humidity : ${weather.simpleWeather.humidity}',
                    style: getStyle(),
                  ),
                ],
              ),
            ],
          ),
          const SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Icon(
                    Icons.water,
                    color: Colors.white,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Text(
                    'Wind speed : ${weather.simpleWeather.windSpeed}',
                    style: getStyle(),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Icon(
                    Icons.sunny,
                    color: Colors.white,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Text(
                    'isDay : ${weather.simpleWeather.isDay ? "Yes" : "No"}',
                    style: getStyle(),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  TextStyle getStyle() {
    return const TextStyle(
      color: Colors.white,
      fontSize: 14,
    );
  }
}
