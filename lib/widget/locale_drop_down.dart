import 'package:flutter/material.dart';

class LocaleDropDown extends StatelessWidget {
  const LocaleDropDown({
    Key? key,
    required this.currentValue,
    required this.values,
    required this.onSelected,
  }) : super(key: key);

  final List<Map> values;
  final String currentValue;
  final Function onSelected;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.only(
        left: size.width * .015,
        right: size.width * .03,
      ),
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(16),
        ),
        gradient: LinearGradient(
          colors: [
            Color(0xFFF3F4F6),
            Color(0xFFF9FAFB),
          ],
        ),
      ),
      width: size.width,
      height: size.height * .07,
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: DropdownButtonHideUnderline(
          child: DropdownButton(
            isDense: true,
            focusColor: Colors.transparent,
            isExpanded: true,
            icon: const Icon(Icons.keyboard_arrow_left_rounded),
            iconSize: 25,
            iconEnabledColor: Color(0xFF9CA3AF),
            iconDisabledColor: Color(0xFF9CA3AF),
            dropdownColor: const Color(0xFFF3F4F6),
            menuMaxHeight: 250,
            borderRadius: const BorderRadius.all(Radius.circular(16)),
            hint: const Text(
              'Languages',
              style: TextStyle(
                color: Colors.grey,
                fontSize: 14,
              ),
            ),
            value: currentValue == '' ? null : currentValue,
            items: values.isNotEmpty
                ? values.map((value) {
                    var reck = DropdownMenuItem(
                      alignment: Alignment.centerRight,
                      value: value['value'],
                      child: Text(
                        value['name'],
                        style: const TextStyle(
                          color: Colors.grey,
                          fontSize: 14,
                        ),
                      ),
                    );
                    return reck;
                  }).toList()
                : null,
            onChanged: (val) {
              onSelected(val.toString());
            },
          ),
        ),
      ),
    );
  }
}
