import 'dart:async';
import 'package:easy_debouncer/easy_debouncer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:speech_to_text/speech_to_text.dart';
import 'package:test_project/controller/weather_controller.dart';
import 'package:test_project/widget/svg_widget.dart';
import 'package:test_project/controller/voice_assistant_controller.dart';
import 'package:translator/translator.dart';

class VoiceAssistantButton extends StatelessWidget {
  VoiceAssistantButton({
    Key? key,
    required this.initRadius,
    required this.color,
    required this.voiceAssistantController,
  }) : super(key: key);
  final double initRadius;
  final Color color;
  final VoiceAssistantController voiceAssistantController;
  final WeatherController weatherController = Get.find<WeatherController>();
  final StreamController<String> command = StreamController();
  final translator = GoogleTranslator();

  bool isShowDialog = false;
  void wave(bool wave) {
    voiceAssistantController.isWave.value = wave;
    voiceAssistantController.isWave.refresh();
    if (wave) {
      voiceAssistantController.startListening();
    } else {
      voiceAssistantController.stopListening();
    }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    voiceAssistantController.setCommandController(command);
    _listenToCommand();
    return Obx(() {
      double radius = voiceAssistantController.isWave.value
          ? initRadius * 1.75
          : initRadius;
      radius /= 1.5;
      double width = (radius - radius / 2) +
          (mapRange(voiceAssistantController.val.value, radius / 5).abs() * 2);
      return AnimatedContainer(
        duration: const Duration(milliseconds: 300),
        height: radius,
        width: radius,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Center(
              child: voiceAssistantController.isWave.value
                  ? AnimatedContainer(
                      duration: const Duration(milliseconds: 100),
                      width: width,
                      height: width,
                      child: FittedBox(
                        child: Lottie.asset(
                          'assets/anim/blob.json',
                        ),
                      ),
                    )
                  : const SizedBox(),
            ),
            GestureDetector(
              onTapDown: (_) => wave(true),
              onTapUp: (_) => wave(false),
              onTapCancel: () => wave(false),
              child: AnimatedContainer(
                duration: const Duration(milliseconds: 300),
                width: radius - radius / 2,
                height: radius - radius / 2,
                padding: const EdgeInsets.all(15),
                decoration: const BoxDecoration(
                  gradient: RadialGradient(colors: [
                    Color(0xFFB91C1C),
                    Color(0xFFEF4444),
                  ], stops: [
                    .1,
                    .75
                  ]),
                  shape: BoxShape.circle,
                ),
                child: const SvgWidget(path: 'assets/icons/mic.svg'),
              ),
            ),
          ],
        ),
      );
    });
  }

  double mapRange(double value, double max) {
    return (value - 30) * (max - 0) / (90 - 30) + 0;
  }

  void _listenToCommand() {
    command.stream.listen((event) {
      if (voiceAssistantController.isTranslate.value) {
        voiceAssistantController.simpleText.value = event;
        voiceAssistantController.simpleText.refresh();
        if (event != '') {
          Debouncer.debounce('translate', const Duration(milliseconds: 500),
              () {
            voiceAssistantController.isTranlating.value = true;
            voiceAssistantController.isTranlating.refresh();
            translator
                .translate(
              event,
              from: 'fa',
              to: voiceAssistantController.currentLocale.value.split('_')[0],
            )
                .then((s) {
              voiceAssistantController.translatedText.value = s.text;
              voiceAssistantController.translatedText.refresh();
              voiceAssistantController.isTranlating.value = false;
              voiceAssistantController.isTranlating.refresh();
            });
          });
        }
      } else {
        if (event != '') {
          print('RTY : $event');
          Debouncer.debounce('weather', const Duration(milliseconds: 500), () {
            translator.translate(event, from: 'fa', to: 'en').then((s) {
              print('LTY : $event');
              weatherController.updateWeather(s.text);
              wave(false);
            });
          });
        }
      }
    });
  }
}
