import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SvgWidget extends StatelessWidget {
  const SvgWidget({
    Key? key,
    required this.path,
    this.color,
    this.fastLoad = false,
  }) : super(key: key);
  final String path;
  final bool fastLoad;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return color != null
        ? SvgPicture.asset(path, color: color)
        : SvgPicture.asset(path);
  }
}
